import jsonData from "./MusicArtists";
import moment from "moment";

export const loadData = () => {
    if(!localStorage.getItem('artistList')){
        const items = JSON.parse(JSON.stringify(jsonData));
        const itemsToSet = items["artist_list"];
        const itemsToSetWithComments = itemsToSet.map( (item) => {
            const curItem  = item["artist"];
            curItem["comments"] = '';
            let date = new Date(curItem["updated_time"]);
            let dateStringWithTime = moment(date).format('YYYY-MM-DD HH:mm:ss');
            curItem["updated_time"] = dateStringWithTime;
            return curItem;
        });
        localStorage.setItem('artistList', JSON.stringify(itemsToSetWithComments));
        console.log("Set");
    }else {
        console.log("Items already in storage");
    }
};

export const getData = () => JSON.parse(localStorage.getItem('artistList'));