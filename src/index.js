import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Components/App';
import { loadData } from "./api";
import 'bootstrap/dist/css/bootstrap.min.css';

loadData();

ReactDOM.render(<App />, document.getElementById('root'));

