import React from 'react';
import '../App.css';
import { getData } from '../api'
import Table from './Table'
import Pagination from './Pagination'
import Modal from './Modal'


class App extends React.Component{
    state = {
        data: getData(),
        sort: 'asc',
        sortField: '',
        searchText: '',
        currentPage: 1,
        postsPerPage: 10,
        isModalOpen: false,
        modalId: null,
    };

    onSort = sortField => {
        const cloneData = this.state.data.slice();
        const sortType = this.state.sort === 'asc' ? 'desc' : 'asc';
        let orderedData = [];
        if(sortField === "artist_rating"){
            orderedData = cloneData.sort( (a,b) => {
                if(sortType === 'asc'){
                    return a[sortField] - b[sortField];
                }
                    return b[sortField] - a[sortField];
            });
        }
        else{
            orderedData = cloneData.sort( (a,b) => {
                let nameA = a[sortField].toLowerCase(), nameB = b[sortField].toLowerCase();
                let nameC = '';
                if(sortType === 'desc'){
                    nameC = nameA;
                    nameA = nameB;
                    nameB = nameC;
                }
                if (nameA < nameB)
                    return -1;
                if (nameA > nameB)
                    return 1;
                return 0;
            });
        }
        this.setState({
            data: orderedData,
            sort: sortType,
            sortField
        })
    };

    handleModal = (id) => {
        this.setState({
            isModalOpen: true,
            modalId: id,
        });
    };

    handleModalClose = () => {
        this.setState({
            isModalOpen: false,
            modalId: null,
        });
    };

    handleSearch = (e) => {
        this.setState({
            searchText: e.target.value,
        });
    };

    searchItems = (list) => {
        let searchVal = this.state.searchText.toLowerCase();
        if (searchVal) {
            return list.filter(a => ((a["artist_name"].toLowerCase().includes(searchVal))));
        }
        return list;
    };

    dataToModal = (id) => {
        const dataItems = this.state.data.slice();
        const item = dataItems.filter( (item) => {
            return item["artist_id"] === id;
        });
        console.log(item);
        return item;

    };


    paginate = (pageNumber) => {
        this.setState({
            currentPage: pageNumber,
        })
    };

    render() {
        const {currentPage, postsPerPage, data, isModalOpen, handleModalClose} = this.state;
        const indexOfLastPost = currentPage * postsPerPage;
        const indexOfFirstPost = indexOfLastPost - postsPerPage;
        const currentPosts = data.slice(indexOfFirstPost, indexOfLastPost);

        return (
            <div className="container">
                <input onChange={this.handleSearch} value={this.searchText} className="form-control col-md-3" type="search"
                       placeholder="Search by name"
                       aria-label="Search"/>
                <Table
                    openModal={this.handleModal}
                    data={currentPosts}
                    onSort={this.onSort}
                    searchItems={this.searchItems}
                />
                {isModalOpen &&
                <Modal>
                    closeModal={handleModalClose}
                    data={data}
                </Modal>}
                <Pagination
                    postsPerPage={postsPerPage}
                    totalPosts={data.length}
                    paginate={this.paginate}
                />

            </div>
        );
    }
}

export default App;
