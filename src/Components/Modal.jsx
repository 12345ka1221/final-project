import React from 'react';

const modalStyle = {
    position: 'fixed',
    zIndex: 100,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    width: '100%',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
};

const Modal = ({data}) => {
    console.log(data);
    return (
        <div style={modalStyle} className="modal">
            <div className="modal-container bg-white py-1 px-lg-4 col-md-9 col-lg-7 m-2 " style={{    overflow: 'auto',
                maxHeight: '100vh'}}>
            </div>
        </div>
    );

};

export default Modal;
