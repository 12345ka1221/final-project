import React from 'react';

const Table = ({openModal, data, onSort, searchItems}) => (
    <table className="table">
        <thead>
        <tr>
            <th>Id</th>
            <th onClick={() => onSort('artist_name')}>Name</th>
            <th>Country</th>
            <th onClick={() => onSort('artist_rating')}>Rating</th>
            <th onClick={() => onSort('updated_time')}>Uploaded at</th>
        </tr>
        </thead>
        <tbody>
        {searchItems(data).map(item =>(
            <tr key={item["artist_id"]}>
                <td onClick={() => {openModal(item["artist_id"])}}>{item["artist_id"]}</td>
                <td>{item["artist_name"]}</td>
                <td>{item["artist_country"]}</td>
                <td>{item["artist_rating"]}</td>
                <td>{item["updated_time"]}</td>
            </tr>
        ))}
        </tbody>
    </table>
);

export default Table